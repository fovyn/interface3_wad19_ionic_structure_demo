import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'home-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  @Input() list: Array<string>;
  @Output('selectItem') selectItemEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {}

  selectItemAction(item: string) {
    this.selectItemEvent.emit(item);
  }
}
